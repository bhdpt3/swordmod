package com.sword.swordmod.biomes;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeAmbience;
import net.minecraft.world.biome.BiomeGenerationSettings;
import net.minecraft.world.biome.BiomeMaker;
import net.minecraft.world.biome.DefaultBiomeFeatures;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.biome.MoodSoundAmbience;
import net.minecraft.world.gen.feature.structure.StructureFeatures;
import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilders;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class ModBiomeMaker extends BiomeMaker {

   private static Biome makeGenericWastelandBiome(ConfiguredSurfaceBuilder<SurfaceBuilderConfig> surfaceBuilder, float depth, float scale) {
      MobSpawnInfo.Builder mobspawninfo$builder = new MobSpawnInfo.Builder();
      DefaultBiomeFeatures.withBatsAndHostiles(mobspawninfo$builder);
      BiomeGenerationSettings.Builder biomegenerationsettings$builder = (new BiomeGenerationSettings.Builder()).withSurfaceBuilder(surfaceBuilder);
      DefaultBiomeFeatures.withBadlandsStructures(biomegenerationsettings$builder);
      biomegenerationsettings$builder.withStructure(StructureFeatures.RUINED_PORTAL_MOUNTAIN);
      DefaultBiomeFeatures.withCavesAndCanyons(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withLavaAndWaterLakes(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withLavaLakes(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withMonsterRoom(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withCommonOverworldBlocks(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withOverworldOres(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withExtraGoldOre(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withDisks(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withBadlandsOakTrees(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withBadlandsGrassAndBush(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withNormalMushroomGeneration(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withBadlandsVegetation(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withLavaAndWaterSprings(biomegenerationsettings$builder);
      DefaultBiomeFeatures.withCommonNetherBlocks(biomegenerationsettings$builder);
      return (new Biome.Builder()).precipitation(Biome.RainType.RAIN).category(Biome.Category.MESA).depth(depth).scale(scale).temperature(0.5F).downfall(1.5F).setEffects((new BiomeAmbience.Builder()).setWaterColor(4312372).setWaterFogColor(4312372).setFogColor(3887386).withSkyColor(11743532).withFoliageColor(10387789).withGrassColor(9470285).setMoodSound(MoodSoundAmbience.DEFAULT_CAVE).build()).withMobSpawnSettings(mobspawninfo$builder.copy()).withGenerationSettings(biomegenerationsettings$builder.build()).build();
   }
      
   public static Biome makeWastelandBiome() {
	   return makeGenericWastelandBiome(ConfiguredSurfaceBuilders.field_244191_w, 0.0F, 1.2F);
   }
}
