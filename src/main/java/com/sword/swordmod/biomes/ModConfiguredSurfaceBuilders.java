//package com.sword.swordmod.biomes;
//
//import com.sword.swordmod.SwordMod;
//
//import net.minecraft.block.Blocks;
//import net.minecraft.util.RegistryKey;
//import net.minecraft.util.ResourceLocation;
//import net.minecraft.util.registry.Registry;
//import net.minecraft.util.registry.WorldGenRegistries;
//import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilder;
//import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
//import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
//import net.minecraftforge.event.RegistryEvent;
//import net.minecraftforge.eventbus.api.EventPriority;
//import net.minecraftforge.eventbus.api.SubscribeEvent;
//import net.minecraftforge.fml.common.Mod;
//import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
//
//public class ModConfiguredSurfaceBuilders {
//	public static final RegistryKey<ConfiguredSurfaceBuilder<?>> WASTELAND = key("wasteland");
//
//	private static RegistryKey<ConfiguredSurfaceBuilder<?>> key(final String name) {
//		return RegistryKey.create(Registry.CONFIGURED_SURFACE_BUILDER_REGISTRY, new ResourceLocation(SwordMod.MODID, name));
//	}
//
//	@Mod.EventBusSubscriber(modid = SwordMod.MODID, bus = Bus.MOD)
//	public static class RegistrationHandler {
//		// Ensure this is run after the SurfaceBuilder DeferredRegister in ModSurfaceBuilders
//		@SubscribeEvent(priority = EventPriority.LOW)
//		public static void register(final RegistryEvent.Register<SurfaceBuilder<?>> event) {
//			register(WASTELAND, WastelandSurfaceBuilder.func_242929_a(SurfaceBuilder.RED_SAND_WHITE_TERRACOTTA_GRAVEL_CONFIG));
//		}
//
//		private static void register(final RegistryKey<ConfiguredSurfaceBuilder<?>> key, final ConfiguredSurfaceBuilder<?> configuredSurfaceBuilder) {
//			Registry.register(WorldGenRegistries.CONFIGURED_SURFACE_BUILDER, key.location(), configuredSurfaceBuilder);
//		}
//	}
//}
