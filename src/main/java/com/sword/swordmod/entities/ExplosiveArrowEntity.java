package com.sword.swordmod.entities;

import com.sword.swordmod.RegistryHandler;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class ExplosiveArrowEntity extends AbstractArrowEntity {

	public ExplosiveArrowEntity(EntityType<? extends ExplosiveArrowEntity> p_i50158_1_, World p_i50158_2_) {
		super(p_i50158_1_, p_i50158_2_);
	}

	public ExplosiveArrowEntity(World worldIn, LivingEntity shooter) {
		super(RegistryHandler.EXPLOSIVEARROWENTITY.get(), shooter, worldIn);
	}

	public ExplosiveArrowEntity(World worldIn, double x, double y, double z) {
		super(RegistryHandler.EXPLOSIVEARROWENTITY.get(), x, y, z, worldIn);
	}
	
	private void explode(double x, double y, double z) {
		world.createExplosion(this, null, null, x, y, z, 3, true, Mode.BREAK);
	    this.setDead();
	}
	   
	protected void onEntityHit(EntityRayTraceResult entity) {
		super.onEntityHit(entity);
	    BlockPos pos = entity.getEntity().getPosition();
	    int x = pos.getX();
	    int y = pos.getY();
	    int z = pos.getZ();
	    this.explode(x, y, z);
	    
	}

	protected void func_230299_a_(BlockRayTraceResult blockRayTrace) {
		super.func_230299_a_(blockRayTrace);
		BlockPos pos = blockRayTrace.getPos();
	    int x = pos.getX();
	    int y = pos.getY();
	    int z = pos.getZ();
	    this.explode(x, y, z);
	}

	@Override
	protected ItemStack getArrowStack() {
		return new ItemStack(RegistryHandler.EXPLOSIVEARROW.get());
	}

	@Override
	public IPacket<?> createSpawnPacket(){
		return NetworkHooks.getEntitySpawningPacket(this);
	}
	
}
