package com.sword.swordmod.entities;


import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.collect.Sets;
import com.sword.swordmod.RegistryHandler;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.Explosion;
import net.minecraft.world.Explosion.Mode;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class AtomicBombEntity extends TNTEntity {
	private int radius = 70;
	Set<BlockPos> affectedBlocks = Sets.newHashSet();

	public AtomicBombEntity(EntityType<? extends TNTEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public AtomicBombEntity(World worldIn, double x, double y, double z, @Nullable LivingEntity igniter) {
		super(worldIn, x, y, z, igniter);
		this.setFuse(1800);
		this.prepareExplosion();
	}
	
	private double getDistanceToBlock(BlockPos pos) {
		double deltaX = this.getPosX() - pos.getX();
		double deltaY = this.getPosY() - pos.getY();
		double deltaZ = this.getPosZ() - pos.getZ();
		return Math.sqrt((deltaX * deltaX) + (deltaY * deltaY) + (deltaZ * deltaZ));
	}
	
	@OnlyIn(Dist.CLIENT)
	private void animate() {
		if (!this.world.isRemote) {
			this.world.addParticle(ParticleTypes.EXPLOSION, true, this.getPosX() + 0.5D, this.getPosY() + 0.5D, this.getPosZ()+3.0D, 0.0D, 0.0D, 0.0D);
			this.world.playSound((PlayerEntity)null, this.getPosX(), this.getPosY(), this.getPosZ(), RegistryHandler.NUCLEAR_EXPLOSION.get(), SoundCategory.BLOCKS, 5000.0F, 1.0F);
		}
	}
	
	@SuppressWarnings("deprecation")
	protected void prepareExplosion() {
		int radius = this.radius;
		for (int z=-radius; z<=radius; z++) {
			for (int x=-radius; x<=radius; x++) {
				for (int y=-radius; y<=radius; y++) {
					BlockPos blockpos = new BlockPos(this.getPosX()+x, this.getPosY()+y, this.getPosZ()+z);
					if (getDistanceToBlock(blockpos) <= radius) {
						BlockState blockstate = this.world.getBlockState(blockpos);
						if (!blockstate.isAir(this.world, blockpos) && blockstate.getBlockHardness(this.world, blockpos) >= 0.0F ) {
							this.affectedBlocks.add(blockpos);
						}
					}
				}
			}
		}
	}

	@Override
	protected void explode() {
		int radius = this.radius;
		this.animate();
		Explosion explosion = new Explosion(this.world, this, this.getPosX(), this.getPosY(), this.getPosZ(), radius, false, Mode.DESTROY);
		float f2 = radius * 1.5F;
		int k1 = MathHelper.floor(this.getPosX() - (double)f2 - 1.0D);
		int l1 = MathHelper.floor(this.getPosX() + (double)f2 + 1.0D);
		int i2 = MathHelper.floor(this.getPosY() - (double)f2 - 1.0D);
		int i1 = MathHelper.floor(this.getPosY() + (double)f2 + 1.0D);
		int j2 = MathHelper.floor(this.getPosZ() - (double)f2 - 1.0D);
		int j1 = MathHelper.floor(this.getPosZ() + (double)f2 + 1.0D);
		List<Entity> list = this.world.getEntitiesWithinAABBExcludingEntity(this, new AxisAlignedBB((double)k1, (double)i2, (double)j2, (double)l1, (double)i1, (double)j1));
		for(int k2 = 0; k2 < list.size(); ++k2) {
			Entity entity = list.get(k2);
			if (!entity.isImmuneToExplosions()) {
				entity.attackEntityFrom(explosion.getDamageSource(), 500.0F);
			}
		}
		
		for(BlockPos blockpos : this.affectedBlocks) {
			BlockState blockstate = this.world.getBlockState(blockpos);
			blockstate.onBlockExploded(this.world, blockpos, explosion);
		}

	}
	
	public void tick() {
		super.tick();
		int fuse = this.getFuse();
		if (fuse > 0) {
			if (fuse <= 30) {
				if (fuse % 3 == 0) {
					this.playShortBeep();
				}
			} else if (fuse <= 100) {
				if (fuse % 10 == 0) {
					this.playShortBeep();
				}
			} else if (fuse <= 500) {
				if (fuse % 15 == 0) {
					this.playLongBeep();
				}
			} else {
				if (fuse % 20 == 0) {
					this.playLongBeep();
				}
			}
		}
	}
	
	private void playShortBeep() {
		this.world.playSound((PlayerEntity)null, this.getPosX(), this.getPosY() + 0.5D, this.getPosZ(), RegistryHandler.DETONATING_SHORT_BEEP.get(), SoundCategory.BLOCKS, 1000.0F, 1.0F);
	}
	
	private void playLongBeep() {
		this.world.playSound((PlayerEntity)null, this.getPosX(), this.getPosY() + 0.5D, this.getPosZ(), RegistryHandler.DETONATING_LONG_BEEP.get(), SoundCategory.BLOCKS, 1000.0F, 1.0F);
	}


	
//	@Override
//	protected void explode() {
//		//this.world.createExplosion(this, this.getPosX(), this.getPosYHeight(0.0625D), this.getPosZ(), 100.0F, Mode.DESTROY);
//		float power = 70.0F;
//		int stepsXY = 4;
//		int stepsZ = 6;
//		int stepIncrement = 15;
//		int offsetX = 0;
//		int offsetY = 0;
//		int offsetZ = 0;
//		this.world.playSound(this.getPosX(), this.getPosYHeight(0.0625D), this.getPosZ(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 100, (1.0F + (this.world.rand.nextFloat() - this.world.rand.nextFloat()) * 0.2F) * 0.7F, true);
//		for (int z=0; z<=stepsXY; z++) {
//			for (int x=0; x<=stepsXY; x++) {
//				for (int y=0; y<=stepsZ; y++) {
//					offsetX = x*stepIncrement;
//					offsetY = y*stepIncrement;
//					offsetZ = z*stepIncrement;
//					this.world.createExplosion(this, this.getPosX()+offsetX, this.getPosYHeight(0.0625D)+offsetY, this.getPosZ()+offsetZ, power, Mode.DESTROY);
//					this.world.createExplosion(this, this.getPosX()-offsetX, this.getPosYHeight(0.0625D)-offsetY, this.getPosZ()-offsetZ, power, Mode.DESTROY);
//				}
//			}
//		}
//	}
	
//	protected void explode() {
//		float power = 100.0F;
//		int radius = 70;
//		int radiusStep = 35;
//		int interval = 200;
//		double offsetX = 0;
//		double offsetY = 0;
//		double offsetZ = 0;
//		this.world.createExplosion(this, this.getPosX(), this.getPosYHeight(0.0625D), this.getPosZ(), power, Mode.DESTROY);
//		for (int r=radiusStep; r<=radius; r=r+radiusStep) {
//			for (double theta=0; theta<=2*Math.PI; theta = theta + interval/(2*Math.PI*r)) {
//				for (double phi=0; phi<=2*Math.PI; phi = phi + interval/(2*Math.PI*r)) {
//					offsetX = r*Math.sin(theta)*Math.cos(phi);
//					offsetY = r*Math.sin(theta)*Math.sin(phi);
//					offsetZ = r*Math.cos(phi);
//					this.world.createExplosion(this, this.getPosX()+offsetX, this.getPosYHeight(0.0625D)+offsetY, this.getPosZ()+offsetZ, power, Mode.DESTROY);
//				}
//			}
//		}
//	}
	
//	public Explosion createExplosion(double x, double y, double z, float size) {
//		Explosion explosion = new Explosion(this.world, this, x, y, z, size, false, Mode.DESTROY);
//		if (net.minecraftforge.event.ForgeEventFactory.onExplosionStart(this.world, explosion)) return explosion;
//		explosion.doExplosionA();
//		explosion.doExplosionB(true);
//		this.world.addParticle(ParticleTypes.EXPLOSION_EMITTER, x, y, z, 1.0D, 0.0D, 0.0D);
//		return explosion;
//	}

}
