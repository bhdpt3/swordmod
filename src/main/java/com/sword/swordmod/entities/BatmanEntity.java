package com.sword.swordmod.entities;


import javax.annotation.Nullable;

import com.sword.swordmod.RegistryHandler;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.BreakDoorGoal;
import net.minecraft.entity.ai.goal.HurtByTargetGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookAtWithoutMovingGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.RangedBowAttackGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

public class BatmanEntity extends MonsterEntity implements IRangedAttackMob {
	private final RangedBowAttackGoal<BatmanEntity> aiArrowAttack = new RangedBowAttackGoal<>(this, 1.0D, 10, 55.0F);
	private final MeleeAttackGoal aiAttackOnCollide = new MeleeAttackGoal(this, 1.2D, true) {
      /**
       * Reset the task's internal state. Called when this task is interrupted by another one
       */
		public void resetTask() {
			super.resetTask();
			BatmanEntity.this.setAggroed(false);
		}

      /**
       * Execute a one shot task or start executing a continuous task
       */
		public void startExecuting() {
			super.startExecuting();
			BatmanEntity.this.setAggroed(true);
		}
	};
	
	public BatmanEntity(EntityType<? extends MonsterEntity> type, World worldIn) {
		super(type, worldIn);
		this.experienceValue = 100;
		//this.setCombatTask();
	}
	
	public BatmanEntity(World worldIn) {
		this(RegistryHandler.BATMAN.get(), worldIn);
	}
   
   @Nullable
   public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
	  spawnDataIn = super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	  this.setCustomNameVisible(false);
	  this.setEquipmentBasedOnDifficulty(difficultyIn);
      this.setEnchantmentBasedOnDifficulty(difficultyIn);
      this.setCanPickUpLoot(true);
      this.setCombatTask();
      return spawnDataIn;
   }
   
   public static AttributeModifierMap.MutableAttribute setAttributes() {
      return MonsterEntity.func_234295_eP_()
    		  .createMutableAttribute(Attributes.FOLLOW_RANGE, 50.0D)
    		  //.createMutableAttribute(Attributes.MOVEMENT_SPEED, (double)1.0F)
    		  .createMutableAttribute(Attributes.ATTACK_DAMAGE, 10.0D)
    		  .createMutableAttribute(Attributes.ARMOR, 5.0D)
    		  .createMutableAttribute(Attributes.MAX_HEALTH, 100.0F)
    		  .createMutableAttribute(Attributes.ATTACK_KNOCKBACK);
   }
   
   protected void registerGoals() {
	  this.goalSelector.addGoal(0, new SwimGoal(this));
      this.targetSelector.addGoal(1, (new HurtByTargetGoal(this)));
      this.goalSelector.addGoal(1, new BreakDoorGoal(this, (difficulty) -> true));
      this.goalSelector.addGoal(4, new MoveTowardsTargetGoal(this, 0.9D, 32.0F));
      this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 1.0D));
      this.goalSelector.addGoal(4, new LookAtWithoutMovingGoal(this, PlayerEntity.class, 3.0F, 1.0F));
      this.goalSelector.addGoal(10, new LookAtGoal(this, PlayerEntity.class, 8.0F));
      this.goalSelector.addGoal(8, new LookRandomlyGoal(this));
      this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, MobEntity.class, 5, false, false, (mob) -> {
    	  return mob instanceof IMob && !(mob instanceof BatmanEntity);
      }));
      this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, JokerEntity.class, 100, false, false, null));
   }
   
   /**
    * Gives armor or weapon for entity based on given DifficultyInstance
    */
   protected void setEquipmentBasedOnDifficulty(DifficultyInstance difficultyIn) {
	 super.setEquipmentBasedOnDifficulty(difficultyIn);
     this.setRandomEquipment(EquipmentSlotType.HEAD, new ItemStack(Items.DIAMOND_HELMET), 0.3F);
     this.setRandomEquipment(EquipmentSlotType.CHEST, new ItemStack(Items.DIAMOND_CHESTPLATE), 0.3F);
     this.setRandomEquipment(EquipmentSlotType.LEGS, new ItemStack(Items.DIAMOND_LEGGINGS), 0.3F);
     this.setRandomEquipment(EquipmentSlotType.FEET, new ItemStack(Items.DIAMOND_BOOTS), 0.3F);
     this.setRandomEquipment(EquipmentSlotType.MAINHAND, new ItemStack(Items.BOW), 1.0F);
   }

   private void setRandomEquipment(EquipmentSlotType slot, ItemStack item, float probability) {
	   if (this.world.rand.nextFloat() < probability) {
		   this.setItemStackToSlot(slot, item);
	   }
   }
   
   public void setCombatTask() {
	   if (this.world != null && !this.world.isRemote) {
		   this.goalSelector.removeGoal(this.aiAttackOnCollide);
		   this.goalSelector.removeGoal(this.aiArrowAttack);
		   ItemStack itemstack = this.getHeldItem(ProjectileHelper.getHandWith(this, Items.BOW));
		   if (itemstack.getItem() instanceof net.minecraft.item.BowItem && this.world.rand.nextFloat() <= 0.6F) {
			   int i = 20;
			   this.aiArrowAttack.setAttackCooldown(i);
			   this.goalSelector.addGoal(3, this.aiArrowAttack);
		   } else {
			   this.goalSelector.addGoal(3, this.aiAttackOnCollide);
		   }
	   }
   }

   @Override
   public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor) {
	   AbstractArrowEntity abstractarrowentity = this.fireArrow();
	   if (this.getHeldItemMainhand().getItem() instanceof net.minecraft.item.BowItem)
		   abstractarrowentity = ((net.minecraft.item.BowItem)this.getHeldItemMainhand().getItem()).customArrow(abstractarrowentity);
	   double d0 = target.getPosX() - this.getPosX();
	   double d1 = target.getPosYHeight(0.3333333333333333D) - abstractarrowentity.getPosY();
	   double d2 = target.getPosZ() - this.getPosZ();
	   double d3 = (double)MathHelper.sqrt(d0 * d0 + d2 * d2);
	   abstractarrowentity.shoot(d0, d1 + d3 * (double)0.2F, d2, 1.6F, 2.0F);
	   this.playSound(SoundEvents.ITEM_CROSSBOW_SHOOT, 1.0F, 1.0F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
	   this.world.addEntity(abstractarrowentity);
   }
   
//   protected void dropSpecialItems(DamageSource source, int looting, boolean recentlyHitIn) {
//	   super.dropSpecialItems(source, looting, recentlyHitIn);
//	   this.inventory.func_233543_f_().forEach(this::entityDropItem);
//   }
   
   /**
    * Fires an arrow
    */
   protected AbstractArrowEntity fireArrow() {
      return new ExplosiveArrowEntity(this.world, this);
   }
   
//   protected AbstractArrowEntity fireArrow(ItemStack arrowStack, float distanceFactor) {
//      return ProjectileHelper.fireArrow(this, arrowStack, distanceFactor);
//   }
   
   public void readAdditional(CompoundNBT compound) {
	   super.readAdditional(compound);
	   this.setCombatTask();
   }
   
   public boolean isImmuneToExplosions() {
	   return true;
   }

   public void setItemStackToSlot(EquipmentSlotType slotIn, ItemStack stack) {
	   if (!(stack.getItem() instanceof net.minecraft.item.AirItem)) {
		   super.setItemStackToSlot(slotIn, stack);
		   if (!this.world.isRemote) {
			   this.setCombatTask();
		   }
	   }
   }
   
   public void writeAdditional(CompoundNBT compound) {
      super.writeAdditional(compound);
      compound.putBoolean("IsImmuneToZombification", true);
   }

}
