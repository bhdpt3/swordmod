package com.sword.swordmod.entities;


import javax.annotation.Nullable;

import com.sword.swordmod.RegistryHandler;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.entity.merchant.villager.WanderingTraderEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

public class JokerEntity extends WanderingTraderEntity {
	
   public JokerEntity(EntityType<JokerEntity> type, World worldIn) {
	   super(type, worldIn);
	}
	
	public JokerEntity(World worldIn) {
		this(RegistryHandler.JOKER.get(), worldIn);
	}

	@Nullable
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
		spawnDataIn = super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
		return spawnDataIn;
	}
	protected void registerGoals() {
		super.registerGoals();
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, BatmanEntity.class, 35.0F, 0.5D, 1.5D));
	}
   
   public static AttributeModifierMap.MutableAttribute setAttributes() {
      return MonsterEntity.func_234295_eP_()
    		  .createMutableAttribute(Attributes.FOLLOW_RANGE, 50.0D)
    		  .createMutableAttribute(Attributes.MOVEMENT_SPEED, (double)1.0F)
    		  .createMutableAttribute(Attributes.ARMOR, 20.0D)
    		  .createMutableAttribute(Attributes.MAX_HEALTH, 10000.0D);
   }
   
}
