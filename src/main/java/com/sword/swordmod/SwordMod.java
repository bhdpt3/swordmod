package com.sword.swordmod;

import java.util.List;
import java.util.function.Supplier;

import com.sword.swordmod.entities.BatmanEntity;
import com.sword.swordmod.renderers.BatmanRenderer;
import com.sword.swordmod.renderers.ExplosiveArrowRenderer;
import com.sword.swordmod.renderers.JokerRenderer;

import net.minecraft.world.gen.GenerationStage;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.ai.attributes.GlobalEntityTypeAttributes;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;

import static net.minecraftforge.common.BiomeDictionary.Type.*;


// The value here should match an entry in the META-INF/mods.toml file
@Mod("swordmod")
public class SwordMod
{
    // Directly reference a log4j logger.
	public static final String MODID = "swordmod";

    public SwordMod() {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    	FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setupGoldenBowAnimation);
    	FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setupRenderer);
    	FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setupEntityAttributes);
        RegistryHandler.init();
    }
    
    private void setupGoldenBowAnimation(FMLClientSetupEvent event) {
        ItemModelsProperties.registerProperty(RegistryHandler.GOLDENBOW.get(), new ResourceLocation("pulling"), (p_239428_0_, p_239428_1_, p_239428_2_) -> {
			return p_239428_2_ != null && p_239428_2_.isHandActive() && p_239428_2_.getActiveItemStack() == p_239428_0_ ? 1.0F : 0.0F;
			
		});
        ItemModelsProperties.registerProperty(RegistryHandler.GOLDENBOW.get(), new ResourceLocation("pull"), (p_239429_0_, p_239429_1_, p_239429_2_) -> {
			if (p_239429_2_ == null) {
				return 0.0F;
			} else {
				return p_239429_2_.getActiveItemStack() != p_239429_0_ ? 0.0F : (float)(p_239429_0_.getUseDuration() - p_239429_2_.getItemInUseCount()) / 20.0F;
			}
		});
    }

    private void setupRenderer(final FMLClientSetupEvent event) {
    	RenderingRegistry.registerEntityRenderingHandler(RegistryHandler.EXPLOSIVEARROWENTITY.get(), ExplosiveArrowRenderer::new);
    	RenderingRegistry.registerEntityRenderingHandler(RegistryHandler.BATMAN.get(), BatmanRenderer::new);
    	RenderingRegistry.registerEntityRenderingHandler(RegistryHandler.JOKER.get(), JokerRenderer::new);
    }
    
    @SuppressWarnings("deprecation")
	private void setupEntityAttributes(final FMLCommonSetupEvent event) {
    	DeferredWorkQueue.runLater(() -> {
    		GlobalEntityTypeAttributes.put(RegistryHandler.BATMAN.get(), BatmanEntity.setAttributes().create());
    		GlobalEntityTypeAttributes.put(RegistryHandler.JOKER.get(), BatmanEntity.setAttributes().create());
    	});
    }
    
    //Change monster spawn
    @SubscribeEvent(priority = EventPriority.HIGH)
    public void onMonsterSpawn(BiomeLoadingEvent event) {
        List<MobSpawnInfo.Spawners> spawns = 
            event.getSpawns().getSpawner(EntityClassification.MONSTER);
        
        spawns.add(new MobSpawnInfo.Spawners(RegistryHandler.BATMAN.get(), 1, 1, 1));
        spawns.add(new MobSpawnInfo.Spawners(RegistryHandler.JOKER.get(), 1, 1, 1));

        // Remove existing Enderman spawn information
        //spawns.removeIf(e -> e.type == EntityType.ENDERMAN);

        // Make Enderman spawns more frequent and add Blaze spawns in all biomes
        //spawns.add(new MobSpawnInfo.Spawners(EntityType.BLAZE, 200, 1, 4));
        //spawns.add(new MobSpawnInfo.Spawners(EntityType.ENDERMAN, 200, 1, 4));
    }
    
    //Add Ores to world gen
    @SubscribeEvent(priority = EventPriority.HIGH)
    public void onUndergroundOreGen(BiomeLoadingEvent event) {
    	List<Supplier<ConfiguredFeature<?, ?>>> configs =
    		event.getGeneration().getFeatures(GenerationStage.Decoration.UNDERGROUND_ORES);
    	// Uranium spawns in from level 8 to 32 in amounts of 8 square shaped
    	configs.add(() ->
    		Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, RegistryHandler.URANIUM_ORE.get().getDefaultState(), 16)).range(4).square().func_242731_b(8)
    	);
    }
    
    //Setup Biomes
	private static void setupBiome(final Biome biome, final BiomeManager.BiomeType biomeType, final int weight, final BiomeDictionary.Type... types) {
		BiomeDictionary.addTypes(key(biome), types);
		BiomeManager.addBiome(biomeType, new BiomeManager.BiomeEntry(key(biome), weight));
	}
	
	@SubscribeEvent
	public static void setupBiomes(final FMLCommonSetupEvent event) {
		event.enqueueWork(() -> {
			setupBiome(RegistryHandler.WASTELAND.get(), BiomeManager.BiomeType.DESERT, 200, COLD, DRY, SANDY, OVERWORLD);
		});
	}
	
	private static RegistryKey<Biome> key(final Biome biome) {
		return RegistryKey.getOrCreateKey(ForgeRegistries.Keys.BIOMES, ForgeRegistries.BIOMES.getKey(biome));
	}
	
    //Implements the Fly Effect on player tick
//    @SubscribeEvent(priority = EventPriority.LOWEST)
//    public void onPlayerJump(KeyInputEvent event) {
//    	PlayerEntity player = Minecraft.getInstance().player;
//    	if (event.getKey() == 32) {
//	    	if (player != null && player instanceof PlayerEntity && !player.isPotionActive(RegistryHandler.FLY.get())) {
//	    		if (!player.isOnGround() && !player.isActualySwimming()) {
//	    			Vector3d currentMotion = player.getMotion();
//	    			Vector3d motion = new Vector3d(currentMotion.getX(), 0.10D, currentMotion.getZ());
//	    			player.setMotion(motion);
//	    		}
//	    	}
//    	}
//    }
   
}
