package com.sword.swordmod;

import com.sword.swordmod.blocks.AtomicBombBlock;
import com.sword.swordmod.entities.AtomicBombEntity;
import com.sword.swordmod.entities.BatmanEntity;
import com.sword.swordmod.entities.ExplosiveArrowEntity;
import com.sword.swordmod.entities.JokerEntity;
import com.sword.swordmod.items.ExplosiveArrow;
import com.sword.swordmod.items.GoldenBow;
import com.sword.swordmod.biomes.ModBiomeMaker;
import com.sword.swordmod.biomes.WastelandSurfaceBuilder;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SwordItem;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
import net.minecraft.item.ItemTier;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SpawnEggItem;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class RegistryHandler {
    // create DeferredRegister object
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, SwordMod.MODID);
    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITIES, SwordMod.MODID);
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, SwordMod.MODID);
    public static final DeferredRegister<SoundEvent> SOUND_EVENTS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, SwordMod.MODID);
    public static final DeferredRegister<Attribute> ATTRIBUTES = DeferredRegister.create(ForgeRegistries.ATTRIBUTES, SwordMod.MODID);
    public static final DeferredRegister<Effect> POTIONS = DeferredRegister.create(ForgeRegistries.POTIONS, SwordMod.MODID);
    public static final DeferredRegister<Biome> BIOMES = DeferredRegister.create(ForgeRegistries.BIOMES, SwordMod.MODID);
    public static final DeferredRegister<SurfaceBuilder<?>> SURFACE_BUILDERS = DeferredRegister.create(ForgeRegistries.SURFACE_BUILDERS, SwordMod.MODID);

    //public static final DeferredRegister<Feature<?>> FEATURES = DeferredRegister.create(ForgeRegistries.FEATURES, SwordMod.MODID);
    
    public static void init() {
        // attach DeferredRegister to the event bus
    	ENTITY_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
    	ATTRIBUTES.register(FMLJavaModLoadingContext.get().getModEventBus());  
    	ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
    	BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
    	SOUND_EVENTS.register(FMLJavaModLoadingContext.get().getModEventBus());
    	POTIONS.register(FMLJavaModLoadingContext.get().getModEventBus());
    	BIOMES.register(FMLJavaModLoadingContext.get().getModEventBus());
    	//FEATURES.register(FMLJavaModLoadingContext.get().getModEventBus());
    }
    
    // register entities
	public static final RegistryObject<EntityType<ExplosiveArrowEntity>> EXPLOSIVEARROWENTITY = ENTITY_TYPES.register("explosive_arrow",
		() -> EntityType.Builder.<ExplosiveArrowEntity>create(ExplosiveArrowEntity::new, EntityClassification.MISC).size(0.5F, 0.5F).build(new ResourceLocation(SwordMod.MODID, "explosive_arrow").toString())
	);

	public static final RegistryObject<EntityType<AtomicBombEntity>> ATOMICBOMBENTITY = ENTITY_TYPES.register("atomic_bomb",
		() -> EntityType.Builder.<AtomicBombEntity>create(AtomicBombEntity::new, EntityClassification.MISC).size(0.5F, 0.5F).build(new ResourceLocation(SwordMod.MODID, "atomic_bomb").toString()));
	
	public static EntityType<BatmanEntity> BATMAN_ENTITY_TYPE = EntityType.Builder.<BatmanEntity>create(BatmanEntity::new, EntityClassification.MONSTER).immuneToFire().size(0.6F, 1.8F).trackingRange(50).build(new ResourceLocation(SwordMod.MODID, "batman").toString());
	public static final RegistryObject<EntityType<BatmanEntity>> BATMAN = ENTITY_TYPES.register("batman", () -> BATMAN_ENTITY_TYPE);

	public static EntityType<JokerEntity> JOKER_ENTITY_TYPE = EntityType.Builder.<JokerEntity>create(JokerEntity::new, EntityClassification.MISC).size(0.6F, 1.8F).trackingRange(50).build(new ResourceLocation(SwordMod.MODID, "joker").toString());
	public static final RegistryObject<EntityType<JokerEntity>> JOKER = ENTITY_TYPES.register("joker", () -> JOKER_ENTITY_TYPE);
	// register entity attributes
	
//	public static final RegistryObject<Attribute> BATMAN_ATTRIBUTES = ATTRIBUTES.register("batman", () ->
//		new GlobalEntityTypeAttributes(RegistryHandler.BATMAN.get(), BatmanEntity.setAttributes().create())
//	);
    
	//register blocks
    public static final RegistryObject<Block> URANIUM_ORE = BLOCKS.register("uranium_ore", () ->
		new Block(AbstractBlock.Properties.create(Material.IRON).hardnessAndResistance(5.0f, 6.0f).sound(SoundType.STONE).setRequiresTool().setLightLevel((state) -> {
		      return 7;
		}))
    );
    
    public static final RegistryObject<Block> URANIUM = BLOCKS.register("uranium_block", () ->
		new Block(AbstractBlock.Properties.create(Material.IRON).hardnessAndResistance(10.0f, 15.0f).sound(SoundType.METAL).setRequiresTool().setLightLevel((state) -> {
		      return 15;
		}))
    );
    
    public static final RegistryObject<Block> ATOMICBOMB = BLOCKS.register("atomic_bomb", () ->
    	new AtomicBombBlock(AbstractBlock.Properties.create(Material.TNT).zeroHardnessAndResistance().sound(SoundType.PLANT))
    );
    
    // world gen features
    
//    public static final RegistryObject<Feature<OreFeatureConfig>> URANIUM_ORE_FEATURE = FEATURES.register("uranium_ore",
//    	() -> new OreFeature((new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, URANIUM_ORE.get().getDefaultState(), 64)).CODEC)
//    );
    
    
//    public static final RegistryObject<Feature<OreFeatureConfig>> URANIUM_ORE_FEATURE = FEATURES.register("uranium_ore",
//    	() -> Feature.ORE.withConfiguration(new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, URANIUM_ORE.get().getDefaultState(), 64))
//    );
        
    // register items
    public static final RegistryObject<Item> FURYSWORD = ITEMS.register("furysword", () ->
    	new SwordItem(ItemTier.NETHERITE, 7, 3, new Item.Properties().group(ItemGroup.COMBAT))
    );
    
    public static final RegistryObject<Item> INFINITYSWORD = ITEMS.register("infinitysword", () ->
		new SwordItem(ItemTier.NETHERITE, 9, 2, new Item.Properties().group(ItemGroup.COMBAT))
    );
    
    public static final RegistryObject<Item> OBSIDIANPICKAXE = ITEMS.register("obsidian_pickaxe", () ->
		new PickaxeItem(ItemTier.NETHERITE, 3, 3, new Item.Properties().group(ItemGroup.TOOLS))
    );
    
    public static final RegistryObject<Item> GOLDENBOW = ITEMS.register("goldenbow", () ->
		new GoldenBow(new Item.Properties().group(ItemGroup.COMBAT))
    );

    public static final RegistryObject<Item> EXPLOSIVEARROW = ITEMS.register("explosive_arrow", () ->
		new ExplosiveArrow(new Item.Properties().group(ItemGroup.COMBAT))
    );

    public static final RegistryObject<Item> URANIUM_ORE_ITEM = ITEMS.register("uranium_ore", () ->
		new BlockItem(URANIUM_ORE.get(), new Item.Properties().group(ItemGroup.BUILDING_BLOCKS))
    );
    
    public static final RegistryObject<Item> URANIUM_ITEM = ITEMS.register("uranium_block", () ->
		new BlockItem(URANIUM.get(), new Item.Properties().group(ItemGroup.BUILDING_BLOCKS))
    );
    
    public static final RegistryObject<Item> ATOMICBOMB_ITEM = ITEMS.register("atomic_bomb", () ->
    	new BlockItem(ATOMICBOMB.get(), new Item.Properties().group(ItemGroup.MISC))
    );
    
    public static final RegistryObject<Item> URANIUM_INGOT_ITEM = ITEMS.register("uranium_ingot", () ->
		new Item(new Item.Properties().group(ItemGroup.MISC))
    );
    
    public static final RegistryObject<SpawnEggItem> BATMAN_SPAWN_EGG = ITEMS.register("batman_spawn_egg", () ->
    	new SpawnEggItem(BATMAN_ENTITY_TYPE, 4996656, 986895, new Item.Properties().group(ItemGroup.MISC))
	);
 
    public static final RegistryObject<SpawnEggItem> JOKER_SPAWN_EGG = ITEMS.register("joker_spawn_egg", () ->
		new SpawnEggItem(JOKER_ENTITY_TYPE, 4996656, 986895, new Item.Properties().group(ItemGroup.MISC))
	);
    
    // Potions
    public static final RegistryObject<Effect> FLY = POTIONS.register("fly", () ->
     	new ModEffect(EffectType.BENEFICIAL, 8356754)
	);
    
    // register sounds
    public static final RegistryObject<SoundEvent> DETONATING_LONG_BEEP = SOUND_EVENTS.register("detonating_long_beep", () ->
    	new SoundEvent(new ResourceLocation(SwordMod.MODID, "detonating_long_beep"))
    );
    
    public static final RegistryObject<SoundEvent> DETONATING_SHORT_BEEP = SOUND_EVENTS.register("detonating_short_beep", () ->
		new SoundEvent(new ResourceLocation(SwordMod.MODID, "detonating_short_beep"))
    );
    
    public static final RegistryObject<SoundEvent> NUCLEAR_EXPLOSION = SOUND_EVENTS.register("nuclear_explosion", () ->
		new SoundEvent(new ResourceLocation(SwordMod.MODID, "nuclear_explosion"))
    );
    
    // register biomes
    
    public static final RegistryObject<SurfaceBuilder<SurfaceBuilderConfig>> WASTELAND_SURFACE_BUILDER = SURFACE_BUILDERS.register("wasteland_surface_builder",
    	() -> new WastelandSurfaceBuilder(SurfaceBuilderConfig.field_237203_a_)
	);
    
    public static final RegistryObject<Biome> WASTELAND = BIOMES.register("wasteland", () ->
    	ModBiomeMaker.makeWastelandBiome()
    );
    
}