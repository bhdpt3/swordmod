package com.sword.swordmod.items;

import com.sword.swordmod.entities.ExplosiveArrowEntity;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ExplosiveArrow extends ArrowItem {

	public ExplosiveArrow(Properties builder) {
		super(builder);
	}
	
   public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
	  AbstractArrowEntity arrowentity = new ExplosiveArrowEntity(worldIn, shooter);
      arrowentity.setFire(100);
      return arrowentity;
   }
}