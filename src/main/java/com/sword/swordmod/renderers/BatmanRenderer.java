package com.sword.swordmod.renderers;

import com.sword.swordmod.SwordMod;
import com.sword.swordmod.entities.BatmanEntity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BatmanRenderer extends LivingRenderer<BatmanEntity, PlayerModel<BatmanEntity>> {
	

	public BatmanRenderer(EntityRendererManager renderManagerIn) {
		super(renderManagerIn, new PlayerModel<>(0.0F, true), 0.5F);
		// TODO Auto-generated constructor stub
	}

	public static final ResourceLocation RES_LOCATION = new ResourceLocation(SwordMod.MODID, "textures/entity/batman.png");

	public ResourceLocation getEntityTexture(BatmanEntity entity) {
		return RES_LOCATION;
	}

}

//public class BatmanRenderer extends LivingRenderer<BatmanEntity, PiglinModel<BatmanEntity>> {
//	
//
//
//
//	public BatmanRenderer(EntityRendererManager rendererManager) {
//		super(rendererManager, new PiglinModel<>(0.0F, 64, 64), 0.5F);
//		// TODO Auto-generated constructor stub
//	}
//}