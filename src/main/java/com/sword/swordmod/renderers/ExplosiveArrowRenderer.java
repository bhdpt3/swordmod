package com.sword.swordmod.renderers;

import com.sword.swordmod.SwordMod;
import com.sword.swordmod.entities.ExplosiveArrowEntity;

import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ExplosiveArrowRenderer extends ArrowRenderer<ExplosiveArrowEntity> {
	public static final ResourceLocation RES_EXPLOSIVE_ARROW = new ResourceLocation(SwordMod.MODID, "textures/entity/projectiles/explosive_arrow.png");

	public ExplosiveArrowRenderer(EntityRendererManager manager) {
		super(manager);
	}

	public ResourceLocation getEntityTexture(ExplosiveArrowEntity entity) {
		return RES_EXPLOSIVE_ARROW;
	}

}