package com.sword.swordmod.blocks;

import javax.annotation.Nullable;

import com.sword.swordmod.entities.AtomicBombEntity;

import net.minecraft.block.BlockState;
import net.minecraft.block.TNTBlock;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class AtomicBombBlock extends TNTBlock {

	public AtomicBombBlock(Properties properties) {
		super(properties);
	}
	
	public void catchFire(BlockState state, World world, BlockPos pos, @Nullable net.minecraft.util.Direction face, @Nullable LivingEntity igniter) {
		explode(world, pos, igniter);
	}
	
	/**
	* Called when this Block is destroyed by an Explosion
	*/
	public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosionIn) {
		if (!worldIn.isRemote) {
			AtomicBombEntity tntentity = new AtomicBombEntity(worldIn, (double)pos.getX() + 0.5D, (double)pos.getY(), (double)pos.getZ() + 0.5D, explosionIn.getExplosivePlacedBy());
			worldIn.addEntity(tntentity);
		}
	}

	public static void explode(World world, BlockPos worldIn) {
		explode(world, worldIn, (LivingEntity)null);
	}

	private static void explode(World worldIn, BlockPos pos, @Nullable LivingEntity entityIn) {
		if (!worldIn.isRemote) {
			AtomicBombEntity tntentity = new AtomicBombEntity(worldIn, (double)pos.getX() + 0.5D, (double)pos.getY(), (double)pos.getZ() + 0.5D, entityIn);
			worldIn.addEntity(tntentity);
			worldIn.playSound((PlayerEntity)null, tntentity.getPosX(), tntentity.getPosY(), tntentity.getPosZ(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
		}
	}

}
